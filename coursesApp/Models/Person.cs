﻿namespace coursesApp.Models
{
	public class Person
	{
		public Person(
			int id, 
			string name, 
			string surname,
			string email) 
			: base(id)
		{
			Name = name;
			Surname = surname;
			Email = email;
		}

		public string Name { get; }

		public string Surname { get; }

		public override string ToString()
		{
			return $"{base.ToString()};{Name};{Surname};{Email}";
		}
	}
}
