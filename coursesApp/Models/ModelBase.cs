﻿namespace coursesApp.Models
{
	public class ModelBase
	{
		public ModelBase(int id)
		{
			Id = id;
		}

		public override string ToString()
		{
			return Id.ToString();
		}
	}
}
