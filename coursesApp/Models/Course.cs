﻿namespace coursesApp.Models
{
	public enum Course
	{
		None = 0,
		First,
		Second,
		Third,
		Fourth
	}
}
