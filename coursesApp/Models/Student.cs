﻿namespace coursesApp.Models
{
	public class Student : Person
	{
		public Student(
			int id, 
			string name, 
			string surname,
			string email, 
			Course course) 
			: base(
				id, 
				name, 
				surname,
				email)
		{
			Course = course;
		}

		public Course Course { get; }

		public override string ToString()
		{
			return $"{base.ToString()};{Course}";
		}
	}
}