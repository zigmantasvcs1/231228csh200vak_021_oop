﻿using coursesApp.Converters;
using coursesApp.Models;
using coursesApp.Repositories;
using Newtonsoft.Json;

namespace coursesApp.UI
{
	internal class LecturersHandler
	{
		private readonly FilesRepository repository;
		private readonly LecturersConverter lecturersConverter;

		public LecturersHandler(
			FilesRepository repository,
			LecturersConverter lecturersConverter)
		{
			this.repository = repository;
			this.lecturersConverter = lecturersConverter;
		}

		public void Handle()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į lektorių aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti lektorius į JSON failą");
				Console.WriteLine("2 - išspausdinti lektorius ekrane");
				Console.WriteLine("3 - sukurti lektorių");
				Console.WriteLine("4 - pakeisti lektorių");
				Console.WriteLine("5 - ištrinti lektorių");

				// viduriai
				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						ExportToJsonFile();
						break;
					case "2":
						PrintLecturersToScreen();
						break;
					case "3":
						CreateLecturer();
						break;
					case "4":
						ChangeLecturer();
						break;
					case "5":
						DeleteLecturer();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame su lektoriais? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private void ExportToJsonFile()
		{
			List<string> lines = repository.Get();

			List<Lecturer> lecturers = lecturersConverter.Convert(lines);

			// from objects to json
			var jsString = JsonConvert.SerializeObject(lecturers);

			ExportToJson(jsString);
			Console.WriteLine("Failas lecturers.json exportuotas");
		}

		private void PrintLecturersToScreen()
		{
			List<string> lines = repository.Get();

			List<Lecturer> lecturers = lecturersConverter.Convert(lines);

			Console.WriteLine("Lektorių sąrašas:");
			Console.WriteLine($"Id;Vardas;Pavardė;Email;Darbuotojo numeris;Ar pensijoj?");
			foreach (var lecturer in lecturers)
			{
				Console.WriteLine(lecturer);
			}
		}

		private void CreateLecturer()
		{
			Console.WriteLine("Suveskite lektoriaus duomenis");

			Console.WriteLine("Iveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Iveskite varda");
			var name = Console.ReadLine();

			Console.WriteLine("Iveskite pavarde");
			var surname = Console.ReadLine();

			Console.WriteLine("Iveskite pazymejimo numeris");
			string employeeId = Console.ReadLine();

			Console.WriteLine("Ar pensijoje? t/n");
			string answer = Console.ReadLine();

			Lecturer lecturer = new Lecturer(
				id,
				name,
				surname,
				$"{name}{surname}@vilniuscoding.lt",
				employeeId,
				answer == "t"
			);

			repository.Save(new List<string> { lecturer.ToString() });

			Console.WriteLine("Lektorius sukurtas");
		}

		private void ChangeLecturer()
		{
			Console.WriteLine("Suveskite lektoriaus duomenis, kuriuos keisite");

			Console.WriteLine("Kokį lektorių keisite? Pasirinkite id iš sąrašo.");
			PrintLecturersToScreen();

			Console.WriteLine("Iveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Iveskite varda");
			var name = Console.ReadLine();

			Console.WriteLine("Iveskite pavarde");
			var surname = Console.ReadLine();

			Console.WriteLine("Iveskite pazymejimo numeris");
			string employeeId = Console.ReadLine();

			Console.WriteLine("Ar pensijoje? t/n");
			string answer = Console.ReadLine();

			List<string> lines = repository.Get();

			List<Lecturer> lecturers = lecturersConverter.Convert(lines);

			Lecturer newLecturer = new Lecturer(
				id,
				name,
				surname,
				$"{name}{surname}@vilniuscoding.lt",
				employeeId,
				answer == "t"
			);

			ReplaceLecturers(lecturers, id, newLecturer);

			repository.Rewrite(
				lecturers
					.Select(x => x.ToString())
					.ToList()
			);

			Console.WriteLine("Lektorius pakeistas");
		}

		private void ReplaceLecturers(
			List<Lecturer> lecturers,
			int id,
			Lecturer newLecturer)
		{
			for (int i = 0; i < lecturers.Count; i++)
			{
				if (lecturers[i].Id == id)
				{
					lecturers[i] = newLecturer;
					break;
				}
			}
		}

		private void DeleteLecturer()
		{
			Console.WriteLine("Kokį lektorių trinsite? Pasirinkite id iš sąrašo.");
			PrintLecturersToScreen();

			Console.WriteLine("Įveskite id");
			var id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				List<string> lines = repository.Get();

				List<Lecturer> lecturers = lecturersConverter.Convert(lines);

				lecturers.RemoveAll(student => student.Id == id);

				repository.Rewrite(
					lecturers
						.Select(x => x.ToString())
						.ToList()
				);

				Console.WriteLine($"Lektorius ištrintas");
			}
			else
			{
				Console.WriteLine("Lektorius nebuvo ištrintas");
			}
		}

		private void ExportToJson(string json)
		{
			using (var streamWriter = new StreamWriter("lecturers.json"))
			{
				streamWriter.WriteLine(json);
			}
		}
	}
}
