﻿using coursesApp.Converters;
using coursesApp.Models;
using coursesApp.Repositories;

namespace coursesApp.UI
{
	public class StudentsHandler
	{
		private readonly FilesRepository repository;
		private readonly StudentsConverter studentsConverter;

		public StudentsHandler(
			FilesRepository repository,
			StudentsConverter studentsConverter)
		{
			this.repository = repository;
			this.studentsConverter = studentsConverter;
		}

		public void Handle()
		{
			throw new NotImplementedException();
		}

		private void ExportToJsonFile()
		{
			throw new NotImplementedException();
		}

		private void PrintStudentsToScreen()
		{
			throw new NotImplementedException();
		}

		private void CreateStudent()
		{
			throw new NotImplementedException();
		}

		private void ChangeStudent()
		{
			throw new NotImplementedException();
		}

		private void DeleteStudent()
		{
			throw new NotImplementedException();
		}

		private void ExportToJson(string json)
		{
			throw new NotImplementedException();
		}

		private void ReplaceStudent(
			List<Student> students, 
			int id, 
			Student newStudent)
		{
			throw new NotImplementedException();
		}
	}
}
