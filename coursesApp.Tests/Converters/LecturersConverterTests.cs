﻿using coursesApp.Converters;

namespace coursesApp.Tests.Converters
{
	[TestClass]
	public class LecturersConverterTests
	{
		private LecturersConverter _lecturersConverter;

		[TestInitialize]
		public void Setup()
		{
			_lecturersConverter = new LecturersConverter();
		}

		[TestMethod]
		public void ConvertWithEmptyLinesShouldReturnEmptyList()
		{
			// Arrange
			var lines = new List<string>();

			// Act
			var result = _lecturersConverter.Convert(lines);

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(0, result.Count);
		}

		[TestMethod]
		public void ConvertWithValidLineShouldReturnLecturer()
		{
			// Arrange
			var lines = new List<string> { "1;John;Doe;JohnDoe@vilniuscoding.lt;Employee123;false" };

			// Act
			var result = _lecturersConverter.Convert(lines);

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(1, result.Count);
			Assert.AreEqual(1, result[0].Id);
			Assert.AreEqual("John", result[0].Name);
			Assert.AreEqual("Doe", result[0].Surname);
			Assert.AreEqual("JohnDoe@vilniuscoding.lt", result[0].Email);
			Assert.AreEqual("Employee123", result[0].EmployeeId);
			Assert.AreEqual(false, result[0].IsRetired);
		}

		[TestMethod]
		[ExpectedException(typeof(FormatException))]
		public void ConvertWithInvalidBoolInLineShouldThrowFormatException()
		{
			// Arrange
			var lines = new List<string> { "1;John;Doe;JohnDoe@vilniuscoding.lt;Employee123;notBool" };

			// Act
			_lecturersConverter.Convert(lines);
		}

		[TestMethod]
		[ExpectedException(typeof(FormatException))]
		public void ConvertWithInvalidIntInLineShouldThrowFormatException()
		{
			// Arrange
			var lines = new List<string> { "notInt;John;Doe;JohnDoe@vilniuscoding.lt;Employee123;false" };

			// Act
			_lecturersConverter.Convert(lines);
		}
	}
}
